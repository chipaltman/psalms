use chrono::prelude::*;
use std::collections::HashMap;
use std::fs;
mod pile;

fn show(path: &str) {
    let file = fs::read_to_string(path).unwrap();
    println!("\n{}", file);
}

fn main() {
    let today = || Local::now().day().to_string();

    let mut day_to_proper = HashMap::new();

    //  Key             |   Value
    //  --------------------------------
    //  day: &str       |   proper: [int]
    //  (of the month)  |   (array of daily psalm numbers)

    day_to_proper.insert("1", ["1", "2", "3", "4", "5", "6" /*, 7*/]);
    day_to_proper.insert("11", ["55", "56", "57", "58", "59", "60"]);

    let mut ps_to_textfile = HashMap::new();
    //  Key             |   Value
    //  --------------------------------
    //  ps: &str        |   textfile: &str
    //  (psalm number)  |   (name of text file)

    // Populate ps_to_textfile: HashMap
    ps_to_textfile.insert("1", "ps01.txt");
    ps_to_textfile.insert("2", "ps02.txt");
    ps_to_textfile.insert("3", "ps03.txt");
    ps_to_textfile.insert("4", "ps04.txt");
    ps_to_textfile.insert("5", "ps05.txt");
    ps_to_textfile.insert("6", "ps06.txt");
    ps_to_textfile.insert("7", "ps07.txt");
    ps_to_textfile.insert("47", "ps47.txt");
    ps_to_textfile.insert("48", "ps48.txt");
    ps_to_textfile.insert("49", "ps49.txt");
    ps_to_textfile.insert("55", "ps55.txt");
    ps_to_textfile.insert("56", "ps56.txt");
    ps_to_textfile.insert("57", "ps57.txt");
    ps_to_textfile.insert("58", "ps58.txt");
    ps_to_textfile.insert("59", "ps59.txt");
    ps_to_textfile.insert("60", "ps60.txt");

    let proper_for_today = day_to_proper
        .get(&today()[..]) // string slice syntax, [..] is full range
        .expect("array value from day_to_proper");

    let mut _cat = String::new();

    // TODO:
    //          ps -> textfile -> content -> push_str into cat
    //          print cat
    
    for ps in proper_for_today.iter() {

        show(ps_to_textfile.get(&ps[..]).expect("name of a textfile"));
    };

//  let content = |ps| {
//
//  }

    //  || { for psalm in proper_for_today.iter() { ps_to_textfile.get(&psalm).expect("path value from ps_textfile"); }};

    // Open psfile and print to stdout
}
