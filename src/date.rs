use chrono::prelude::*;
use dashmap::DashMap;

pub fn get_day() {
    let day: String = Local::now().day().to_string(); 
    println!("{}", day);
}

pub fn dash_map() {
    let psalter = DashMap::new();

    psalter.insert(
    "1".to_string(),
    "Psalms 1 through 5 (Morning)\nPsalms 6 through 8 (Evening)".to_string(),
    );

    psalter.insert(
    "6".to_string(),
    "Psalms 30 through 31 (Morning)\nPsalms 32 through 33 (Evening)".to_string(),
    );


    psalter.insert(
    "8".to_string(),
    "Psalms 38 through 40 (Morning)\nPsalms 41 through 43 (Evening)".to_string(),
    );

    psalter.insert(
    "18".to_string(),
    "Psalms 90 through 92 (Morning)\nPsalms 93 through 94 (Evening)".to_string(),
    );

}


// date to psalm
