#![allow(unused)]
#![allow(dead_code)]
use chrono::prelude::*;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::Read;
use std::path::Path;
use std::result::Result;
use toml::Value;

pub fn show(path: &str) {
    let file = fs::read_to_string(path).unwrap();
    println!("\n{}", file);
}

pub fn get_day() /* ->  String */
{
    let day: String = Local::now().day().to_string();
}

pub fn open_file(/* path: Path */) -> std::io::Result<File> {
    let value = File::open("../data/cycle_evening.toml");
    value
}

fn example_toml() {
    let value = "foo = 'bar'".parse::<Value>().unwrap();
    assert_eq!(value["foo"].as_str(), Some("bar"));
}

// pub fn load_psalm_cycle(/* day: &str */) -> toml::value::Value {
//     Loader::from_file(Path::new("../data/psalm_cycle.toml")).unwrap()
// }

// pub fn load_hash() {
//     let psalm_cycle: String =
//         Loader::from_file(Path::new("../data/psalm_cycle.toml")).expect("BAD TOML");
//     println!("{:#?}", &psalm_cycle);
// }
//
// fn _open_file() {
//     let file = lib::open_file().unwrap();
//     println!("{:#?}", file);
// }

//
// StructOpt Stuff
//
// #[derive(Debug, StructOpt)]
// #[structopt(name = "psalms", about = "Read the psalms in your terminal.")]
// struct Opt {
//     /// Set speed
//     // we don't want to name it "speed", need to look smart
//     #[structopt(short = "v", long = "velocity", default_value = "42")]
//     speed: f64,
//
//     /// Input file
//     #[structopt(parse(from_os_str))]
//     input: PathBuf,
//
//     /// Output file, stdout if not present
//     #[structopt(parse(from_os_str))]
//     output: Option<PathBuf>,
// }
//
// fn _with_opt() {
//    let opt = Opt::from_args();
//    println!(
//        "\n
//        {:?}",
//        opt
//    );
//  }
//
