The 30-Day Cycle of Psalms
==========================
*Reading through the Psalms each month is a traditional Christian practice, and a good excuse to write a cli app in Rust.*  

**This is a learning project.**  
As of today `2020.03.10` it is in early planning.  
I'm an amateur so it will take some time. 

Progress
--------

- [x] print psalm text from file to stdout
- [ ] create a separate file for each psalm (e.g. "01_psalm.txt")
- [x] choose path from `psnum_path` hashmap
- [ ] add parameter for choosing a psalm by number


Use of Psalms in the Daily Office
----------------------------------

> The recitation of the Psalms is central to daily worship throughout the whole of Christian Tradition. Anglicanism at the time of the Reformation established that the entire Psalter should be read in the Daily Office once each month, according to the pattern printed with this Lectionary. Contemporary practice sometimes lessens the number of the daily psalms, which practice is permissible as long as the entire Psalter is regularly read. For any day, the psalms appointed may be reduced in number according to local circumstance. If there is only one office, the psalms may be drawn from those appointed for Morning or Evening that Day. If a two month cycle of psalms is desired, the Morning psalms may be read in one month and the Evening psalms in the next. When there is a thirty-first day of the month, psalms from the Psalms of Ascents, Psalms 120 to 134, are used. 

*From THE BOOK OF COMMON PRAYER (2019)*  
*Copyright © 2019 by the Anglican Church in North America*


CONSIDERATIONS: 
----------------
### Path vs. File `2020.03.10`  
My function can find files in the project root, but if I move them into a module it cannot 🤔  

One might be forgiven for hoping `"../data/01_psalm.txt"` would do the trick — I suspect it is a Type issue.

### Start From the End `2020.03.09`
I'm learning to code, and learning Rust. So far I have had problems related to Types and maybe an old crate.
I know better than to write code while the logic is still up in the air. But I did it anyway. I shall now work backward; first by ensuring I can read a file to stout. *Then* I'll select the file through a key/value pair. Getting the right key is trivial, so at that point I'll be pretty much done.

Logic
-----

- [ ] chooses file(s) in `num_text` hashmap
- [x] reads psalm text from file(s)
- [x] prints psalm text to stdout

+ [ ] https://doc.rust-lang.org/stable/rust-by-example/std/hash/alt_key_types.html#alternatecustom-key-types

psalms
Prints psalms to stdout

Default according to current datetime.

    -d, --day
    -e, --evening  
    -m, --morning  
    -p, --psalm
    -t, --text

    -v, --verse-range

