Day 1 : Morning Prayer

I - Beatus vir qui non abiit

Blessed is the man who has not walked in the counsel of the ungodly, ♦
nor stood in the way of sinners, and has not sat in the seat of the scornful;

But his delight is in the law of the Lord, ♦
and on his law will he meditate day and night.

And he shall be like a tree planted by the waterside, ♦
that will bring forth his fruit in due season.

His leaf also shall not wither; ♦
and look, whatever he does, it shall prosper.

As for the ungodly, it is not so with them; ♦
but they are like the chaff, which the wind scatters away from the face of the earth.

Therefore the ungodly shall not be able to stand in the judgment, ♦
neither the sinners in the congregation of the righteous.

For the Lord knows the way of the righteous, ♦
but the way of the ungodly shall perish.
