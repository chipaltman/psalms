    Psalm 48

1   Great is the Lord and highly to be praised, ♦
        in the city of our God.

2   His holy mountain is fair and lifted high, ♦
        the joy of all the earth.

3   On Mount Zion, the divine dwelling place, ♦
        stands the city of the great king.

4   In her palaces God has shown himself ♦
        to be a sure refuge.

5   For behold, the kings of the earth assembled ♦
        and swept forward together.

6   They saw, and were dumbfounded; ♦
        dismayed, they fled in terror.

7   Trembling seized them there;
    they writhed like a woman in labour, ♦
        as when the east wind shatters the ships of Tarshish.

8   As we had heard, so have we seen
    in the city of the Lord of hosts, the city of our God: ♦
        God has established her for ever.

9   We have waited on your loving-kindness, O God, ♦
        in the midst of your temple.

10  As with your name, O God,
    so your praise reaches to the ends of the earth; ♦
        your right hand is full of justice.

11  Let Mount Zion rejoice and the daughters of Judah be glad, ♦
        because of your judgements, O Lord.

12  Walk about Zion and go round about her;
    count all her towers; ♦
        consider well her bulwarks; pass through her citadels,

13  That you may tell those who come after
    that such is our God for ever and ever. ♦
        It is he that shall be our guide for evermore.
